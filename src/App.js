import logo from './logo.svg';
import './App.css';
import HeaderCoponent from './BaiTapThucHanhLayout/HeaderCoponent';
import BodyComponent from './BaiTapThucHanhLayout/body/BodyComponent';
import FooterCoponent from './BaiTapThucHanhLayout/footer/FooterCoponent';

function App() {
  return (
    <div className="App">
      <HeaderCoponent />
      <BodyComponent />
      <FooterCoponent />
    </div>
  );
}

export default App;
