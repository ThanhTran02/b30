import React from 'react'

const HeaderCoponent = () => {
    return (
        <div className=" bg-black p-5 text-white text-xl">
            <div className='container flex mx-auto px-20'>
                <div className="logo mr-auto">Start Bootstrap</div>
                <div className="header__content">
                    <a href="" className='mr-3'>Home</a>
                    <a href="" className='mr-3 text-gray-500 hover:text-gray-300 duration-500'>About</a>
                    <a href="" className='mr-3 text-gray-500 hover:text-gray-300 duration-500'>Contact</a>
                </div>
            </div>
        </div>
    )
}

export default HeaderCoponent