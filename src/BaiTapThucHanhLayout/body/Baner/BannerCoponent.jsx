import React from 'react'

const BannerCoponent = () => {
    return (
        <div className="container mx-auto w-4/5 p-28 bg-slate-100 mt-20 ">
            <div className='flex flex-col items-center justify-center p-28'>
                <h1 className='font-bold text-5xl p-4'>A warm welcome!</h1>
                <p className='text-3xl p-4'>Bootstrap utility classes are used to create this jumbotron since the old component has been removed from the framework. Why create custom CSS when you can use utilities?</p>
                <button class="w-24 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                    Button
                </button>
            </div>
        </div>
    )
}

export default BannerCoponent