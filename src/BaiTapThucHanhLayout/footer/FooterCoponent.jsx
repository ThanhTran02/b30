import React from 'react'

const FooterCoponent = () => {
    return (
        <div className=' bg-black '>
            <h3 className='text-white text-center p-5 text-xm'>Copyright © Your Website 2023</h3>
        </div>
    )
}

export default FooterCoponent