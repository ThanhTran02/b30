import React from 'react'
import BannerCoponent from './Baner/BannerCoponent'
import ItemCoponent from './Item/ItemCoponent'

const BodyComponent = () => {
    return (
        <div >
            <BannerCoponent />
            <ItemCoponent />
        </div>
    )
}

export default BodyComponent