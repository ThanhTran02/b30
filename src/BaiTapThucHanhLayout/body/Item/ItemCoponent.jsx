import React from 'react'

const ItemCoponent = () => {
    return (
        <div className='container mx-auto px-20 pt-20 mb-20'>
            <div className='grid grid-cols-3 '>
                <div className=' rounded-lg p-5 m-4 gap-4 text-center items-center justify-center flex flex-col bg-orange-100'>
                    <div className=' bg-cyan-500 w-14 h-14 items-center justify-center flex  rounded-lg'  >
                        <i class="fab fa-android texr-5xl text-white"></i>
                    </div>
                    <h2 className='text-2xl text-emerald-400 font-bold p-3' >Fresh new layout</h2>
                    <p>With Bootstrap 5, we've created a fresh new layout for this template!</p>
                </div>
                <div className='rounded-lg p-5 m-4 gap-4 text-center items-center justify-center flex flex-col bg-orange-100'>
                    <div className=' bg-cyan-500 w-14 h-14 items-center justify-center flex  rounded-lg'  >
                        <i class="fab fa-android texr-5xl text-white"></i>
                    </div>
                    <h2 className='text-2xl text-emerald-400 font-bold p-3' >Fresh new layout</h2>
                    <p>With Bootstrap 5, we've created a fresh new layout for this template!</p>
                </div>
                <div className='rounded-lg p-5 m-4  gap-4 text-center items-center justify-center flex flex-col bg-orange-100'>
                    <div className=' bg-cyan-500 w-14 h-14 items-center justify-center flex  rounded-lg'  >
                        <i class="fab fa-android texr-5xl text-white"></i>
                    </div>
                    <h2 className='text-2xl text-emerald-400 font-bold p-3' >Fresh new layout</h2>
                    <p>With Bootstrap 5, we've created a fresh new layout for this template!</p>
                </div>
                <div className='rounded-lg p-5 m-4 gap-4 text-center items-center justify-center flex flex-col bg-orange-100'>
                    <div className=' bg-cyan-500 w-14 h-14 items-center justify-center flex  rounded-lg'  >
                        <i class="fab fa-android texr-5xl text-white"></i>
                    </div>
                    <h2 className='text-2xl text-emerald-400 font-bold p-3' >Fresh new layout</h2>
                    <p>With Bootstrap 5, we've created a fresh new layout for this template!</p>
                </div>
                <div className='rounded-lg p-5 m-4 gap-4 text-center items-center justify-center flex flex-col bg-orange-100'>
                    <div className=' bg-cyan-500 w-14 h-14 items-center justify-center flex  rounded-lg'  >
                        <i class="fab fa-android texr-5xl text-white"></i>
                    </div>
                    <h2 className='text-2xl text-emerald-400 font-bold p-3' >Fresh new layout</h2>
                    <p>With Bootstrap 5, we've created a fresh new layout for this template!</p>
                </div>
                <div className='rounded-lg p-5 m-4 gap-4 text-center items-center justify-center flex flex-col bg-orange-100'>
                    <div className=' bg-cyan-500 w-14 h-14 items-center justify-center flex  rounded-lg'  >
                        <i class="fab fa-android texr-5xl text-white"></i>
                    </div>
                    <h2 className='text-2xl text-emerald-400 font-bold p-3' >Fresh new layout</h2>
                    <p>With Bootstrap 5, we've created a fresh new layout for this template!</p>
                </div>
            </div>
        </div>
    )
}

export default ItemCoponent